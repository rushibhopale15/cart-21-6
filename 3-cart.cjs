const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

//Q1. Find all the items with price more than $65.


function findItemsMoreThan65(products) {
    let requiredCartItems = [];
    products.map((productsObject) => {
        Object.keys(productsObject).map((items) => {
            if (productsObject[items].price !== undefined) {
                if (productsObject[items].price.replace('$', '') > 65) {
                    requiredCartItems.push(productsObject[items]);
                }
            }
            else {
                productsObject[items].map((innerItem) => {
                    Object.keys(innerItem).map((inneritemName) => {
                        if (innerItem[inneritemName].price.replace('$', '') > 65) {
                            requiredCartItems.push(innerItem);
                        }
                    })
                });
            }
        })
    })
    console.log(requiredCartItems);
}
findItemsMoreThan65(products)


//Q2. Find all the items where quantity ordered is more than 1

function findItemsMoreThan1Quantity(products) {

    let requiredCartItems = [];
    products.map((productsObject) => {
        Object.keys(productsObject).map((items) => {
            if (productsObject[items].quantity !== undefined) {
                if (productsObject[items].quantity > 1) {
                    requiredCartItems.push(productsObject[items]);
                }
            }
            else {
                productsObject[items].map((innerItem) => {
                    Object.keys(innerItem).map((inneritemName) => {
                        if (innerItem[inneritemName].quantity > 1) {
                            requiredCartItems.push(innerItem);
                        }
                    })
                });
            }
        })
    })
    console.log(requiredCartItems);
}

findItemsMoreThan1Quantity(products)